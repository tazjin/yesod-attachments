{-# LANGUAGE FlexibleContexts           #-}
{-# LANGUAGE FlexibleInstances          #-}
{-# LANGUAGE GADTs                      #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# LANGUAGE KindSignatures             #-}
{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE RecordWildCards            #-}
{-# LANGUAGE ScopedTypeVariables        #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}
module Yesod.Attachments where

import           Codec.Binary.Base64.String
import qualified Control.Exception          as CE
import           Control.Monad.Trans.Class  (lift)
import           Control.Monad.Trans.Maybe
import           Data.ByteString            (ByteString)
import qualified Data.ByteString.Char8      as BS
import           Data.Text                  (Text, pack, unpack)
import qualified Data.Text                  as T
import qualified Data.Text.Encoding         as TE
import           Database.Persist.Store
import           Language.Haskell.TH.Syntax (Pred (ClassP), Type (VarT), mkName)
import           System.Directory           (doesFileExist, removeFile)
import           Yesod                      hiding (lift)

data YesodAttachment = YesodAttachment

data File = File FileName FileType

-- |Newtype wrappers to ensure that URLs are generated correctly.
--  These should be used in the Persist model as well.
newtype FileName = FileName { unFileName :: Text}
    deriving (Eq, Show, Read, PathPiece, PersistField)
newtype FileURL = FileURL { unFileURL :: Text }
    deriving (Eq, Show, Read, PathPiece, PersistField)
newtype FileType = FileType { unFileType :: Text }
    deriving (Eq, Show, Read, PathPiece, PersistField)

class ( Yesod m
      , YesodPersist m
      , PersistQuery (YesodPersistBackend m) (GHandler YesodAttachment m)
      , RedirectUrl m (Route m)
      , PathPiece (FileKey m))
       => YesodAttachments m where
    -- |Persistent ID
    type FileKey m
    -- |Filesystem location to store uploads. (default: uploads/)
    attachmentPath :: m -> FilePath
    attachmentPath _ = "uploads/"
    -- |How to construct the upload path from the 'FileKey'. The default
    --  implementation uses the 'PathPiece' instance of the Persistent key
    --  to store files in the 'attachmentPath' folder.
    uploadPath :: PathPiece (FileKey m)
               => m
               -> FileKey m
               -> FilePath
    uploadPath m key = let fn = T.unpack $ toPathPiece key
                       in (attachmentPath m) ++ fn
    -- |Parses a key from a 'Text'. The easiest implementation is using
    --  'fromPathPiece' which is implemented for all Persistent keys.
    parseKey :: m -> FileURL -> Maybe (FileKey m)
    -- |This should be a generic error page saying \"Not found/not allowed\"
    routeToError  :: m -> Route m
    -- |Returns the link to an attachment.
    getAttachmentLink :: FileKey m
                      -> GWidget sub m ()
    -- |Insert a file into the DB
    insertFile :: File -> GHandler sub m (FileKey m)
    -- |Delete a file from the DB
    deleteFromDB :: FileKey m -> GHandler sub m ()
    -- |Checks for file permissions if necessary. A @Just ()@ means
    --  that access is allowed. (default: True)
    fileAccess :: m -> FileKey m -> GHandler sub m Bool
    fileAccess _  _ = return True
    -- |Privilege check on whether the file can be deleted. (default: True)
    deletePrivilege :: m -> FileKey m -> GHandler sub m Bool
    deletePrivilege _  _ = return True


-- |Encoding 'Text' with base64.
encb64 :: Text -> FileType
encb64 = FileType . pack . encode . unpack

-- |Decode a 'FileType'.
decb64 :: FileType -> ByteString
decb64 = BS.pack . decode . unpack . unFileType

mkYesodSub "YesodAttachment"
           [ ClassP ''YesodAttachments [VarT $ mkName "master"] ]
           [parseRoutes|
/#FileURL/#FileType/#FileName FileR GET
|]

-- |Uploads a file. This takes a 'FileInfo' as its argument which should
--  be the result of a 'fileAFormReq'. For optional file uploads see
--  the function 'uploadFileOpt'.
uploadFile :: YesodAttachments m
           => FileInfo
           -> GHandler sub m (FileKey m)
uploadFile fileraw = do
  let file = File (FileName $ fileName fileraw)
                  (encb64 $ fileContentType fileraw)
  filekey <- insertFile file
  master  <- getYesod
  liftIO $ fileMove fileraw (uploadPath master filekey)
  return filekey

-- |Optionally uploads a file.
uploadFileOpt :: YesodAttachments m
              => Maybe FileInfo
              -> GHandler YesodAttachment m (Maybe (FileKey m))
uploadFileOpt Nothing  = return Nothing
uploadFileOpt (Just p) = uploadFile p >>= return . Just

-- |This serves a single file and does not touch the DB.
getFileR :: ( YesodAttachments m
            , HasReps (ContentType, Content))
         => FileURL  -- ^ File ID
         -> FileType -- ^ Content type
         -> FileName -- ^ File name (discarded)
         -> GHandler YesodAttachment m (ContentType, Content)
getFileR key ct _ = do
    master <- getYesod
    filePath <- runMaybeT $ do
        fileKey <- MaybeT (return $ parseKey master key)
        path    <- MaybeT (return $ Just $ uploadPath master fileKey)
        access  <- lift $ fileAccess master fileKey
        fileExists <- liftIO $ doesFileExist path
        if fileExists && access
            then return path
            else fail ""
    let ctype = decb64 ct
    case filePath of
        Nothing  -> redirect $ routeToError master
        (Just p) -> return (ctype, ContentFile p Nothing)

-- |Deletes a file. Returns @True@ if the file was successfully removed from
--  the database and the uploads folder.
deleteFile :: YesodAttachments m
           => FileKey m -- ^ File ID
           -> GHandler sub m Bool
deleteFile fileKey = do
    master <- getYesod
    file <- runMaybeT $ do
        path    <- MaybeT (return $ Just $ uploadPath master fileKey)
        access  <- lift $ deletePrivilege master fileKey
        fileExists <- liftIO $ doesFileExist path
        if fileExists && access
            then return path
            else fail ""
    case file of
      Nothing   -> return False
      Just path -> do
          deleted <- liftIO $ CE.catch (removeFile path >> return True)
                                       (\e -> return $ const False (e :: CE.IOException))
          if deleted
            then deleteFromDB fileKey >> return True
            else return False

